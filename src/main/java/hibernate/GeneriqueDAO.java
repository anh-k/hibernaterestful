package hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

public class GeneriqueDAO<T, S extends Serializable> implements IGeneriqueDAO<T, S> {

    private Class<T> persistentClass;
    private Session session;

    public GeneriqueDAO(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
        this.session = HibernateUtils.getSessionFactory().getCurrentSession();
    }

    @Override
    public T getById(S id) {

        T entity;
        session.beginTransaction();
        entity = session.get(persistentClass, id);
        session.getTransaction().commit();

        return entity;
    }

    @Override
    public List<T> getAll() {
        List<T> objects = null;
        try {

            session.beginTransaction();
            Query query = session.createQuery("from " + persistentClass.getSimpleName());
            objects = ((org.hibernate.query.Query) query).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return objects;
    }

    @Override
    public List<T> getByCriteria(Criterion... criterions) {
        session.beginTransaction();
        Criteria criteria = session.createCriteria(persistentClass);

        for (Criterion criterion : criterions) {
            criteria.add(criterion);
        }

        List<T> retour = criteria.list();
        session.getTransaction().commit();
        return retour;
    }

    @Override
    public List<T> getByExample(T example) {
        return null;
    }

    @Override
    public void insert(T classInstance) {

    }

    @Override
    public void update(T classInstance) {

    }

    @Override
    public void delete(T classInstance) {

    }
}

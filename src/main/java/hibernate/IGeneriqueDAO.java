package hibernate;

import org.hibernate.criterion.Criterion;
import java.io.Serializable;
import java.util.List;

public interface IGeneriqueDAO<T, S extends Serializable> {

    T getById(S id);

    List<T> getAll();

    List<T> getByCriteria(Criterion... criterions);

    List<T> getByExample(T example);

    void insert(T classInstance);

    void update(T classInstance);

    void delete(T classInstance);
}

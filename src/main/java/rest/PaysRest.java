package rest;

import hibernate.GeneriqueDAO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.PaysEntity;
import org.hibernate.criterion.Restrictions;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/pays")
@Tag(name = "pays")
@Produces(MediaType.APPLICATION_JSON)
public class PaysRest {


    @GET
    public Response getPays() {

        try {
            GeneriqueDAO<PaysEntity, Integer> paysDao = new GeneriqueDAO<>(PaysEntity.class);
            List<PaysEntity> paysList = (List) paysDao.getAll();

            return Response.ok(new GenericEntity<List<PaysEntity>>(paysList) {
            }).build();
        } catch (Exception e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }


    @GET
    @Path("{id}")
    public Response getPaysById(@PathParam("id") Integer id) {
        try {
            GeneriqueDAO<PaysEntity, Integer> paysDao = new GeneriqueDAO<>(PaysEntity.class);
            PaysEntity paysSearched = paysDao.getById(id);

            return Response.ok(paysSearched).build();
        } catch (Exception e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }
        return Response.status(Response.Status.NO_CONTENT).build();

    }


    @GET
    @Operation(summary = "Retourne la liste des pays que contient la châine de caractères")
    @Path("/like")
    public Response getPaysLike(@QueryParam("Message") String criteria) {

        GeneriqueDAO<PaysEntity, String> paysDao = new GeneriqueDAO<PaysEntity, String>(PaysEntity.class);
        List listPays;
        if (criteria != null)
            listPays = paysDao.getByCriteria(Restrictions.like("nomPays", "%" + criteria + "%"));

        else
            listPays = paysDao.getAll();
        return Response.ok(new GenericEntity<>(listPays) {
        }).build();

    }

}

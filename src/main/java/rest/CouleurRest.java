package rest;

import hibernate.GeneriqueDAO;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.CouleurEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.util.List;
@Path("/couleurs")
@Tag(name = "Liste des couleurs")
@Produces(MediaType.APPLICATION_JSON)
public class CouleurRest {

    @GET
    public Response getCouleurs() {
        try {
            GeneriqueDAO<CouleurEntity, Integer> couleurDao = new GeneriqueDAO<>(CouleurEntity.class);
            List<CouleurEntity> couleurList = (List) couleurDao.getAll();


            return Response.ok(new GenericEntity<List<CouleurEntity>>(couleurList){}).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}

package rest;

import hibernate.GeneriqueDAO;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.ContinentEntity;
import javax.ws.rs.*;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/continents")
@Tag(name ="continents")
@Produces(MediaType.APPLICATION_JSON)
public class ContinentRest {

    @GET
    public Response getContinent() {
        System.out.println("ALLO");
        try {

            GeneriqueDAO<ContinentEntity, Integer> continentDao = new GeneriqueDAO<>(ContinentEntity.class);
            List<ContinentEntity> continentList = continentDao.getAll();

            return Response.ok(new GenericEntity<List<ContinentEntity>>(continentList){}).build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.NO_CONTENT).build();

    }






}

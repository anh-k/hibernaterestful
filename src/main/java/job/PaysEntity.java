package job;

import javax.persistence.*;

@Entity
@Table(name = "PAYS", schema = "dbo", catalog = "SDBM")
public class PaysEntity {
    private int idPays;
    private String nomPays;

    @Id
    @Column(name = "ID_PAYS")
    public int getIdPays() {
        return idPays;
    }

    public void setIdPays(int idPays) {
        this.idPays = idPays;
    }

    @Basic
    @Column(name = "NOM_PAYS")
    public String getNomPays() {
        return nomPays;
    }

    public void setNomPays(String nomPays) {
        this.nomPays = nomPays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaysEntity that = (PaysEntity) o;

        if (idPays != that.idPays) return false;
        if (nomPays != null ? !nomPays.equals(that.nomPays) : that.nomPays != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPays;
        result = 31 * result + (nomPays != null ? nomPays.hashCode() : 0);
        return result;
    }
}

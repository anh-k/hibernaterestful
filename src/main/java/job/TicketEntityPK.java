package job;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TicketEntityPK implements Serializable {
    private int annee;
    private int numeroTicket;

    @Column(name = "ANNEE")
    @Id
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Column(name = "NUMERO_TICKET")
    @Id
    public int getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(int numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketEntityPK that = (TicketEntityPK) o;

        if (annee != that.annee) return false;
        if (numeroTicket != that.numeroTicket) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = annee;
        result = 31 * result + numeroTicket;
        return result;
    }
}

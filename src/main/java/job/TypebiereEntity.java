package job;

import javax.persistence.*;

@Entity
@Table(name = "TYPEBIERE", schema = "dbo", catalog = "SDBM")
public class TypebiereEntity {
    private int idType;
    private String nomType;

    @Id
    @Column(name = "ID_TYPE")
    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    @Basic
    @Column(name = "NOM_TYPE")
    public String getNomType() {
        return nomType;
    }

    public void setNomType(String nomType) {
        this.nomType = nomType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypebiereEntity that = (TypebiereEntity) o;

        if (idType != that.idType) return false;
        if (nomType != null ? !nomType.equals(that.nomType) : that.nomType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idType;
        result = 31 * result + (nomType != null ? nomType.hashCode() : 0);
        return result;
    }
}

package job;

import javax.persistence.*;

@Entity
@Table(name = "ARTICLE", schema = "dbo", catalog = "SDBM")
public class ArticleEntity {
    private int idArticle;
    private String nomArticle;
    private double prixAchat;
    private Integer volume;
    private Double titrage;

    @Id
    @Column(name = "ID_ARTICLE")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Basic
    @Column(name = "NOM_ARTICLE")
    public String getNomArticle() {
        return nomArticle;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    @Basic
    @Column(name = "PRIX_ACHAT")
    public double getPrixAchat() {
        return prixAchat;
    }

    public void setPrixAchat(double prixAchat) {
        this.prixAchat = prixAchat;
    }

    @Basic
    @Column(name = "VOLUME")
    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    @Basic
    @Column(name = "TITRAGE")
    public Double getTitrage() {
        return titrage;
    }

    public void setTitrage(Double titrage) {
        this.titrage = titrage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleEntity that = (ArticleEntity) o;

        if (idArticle != that.idArticle) return false;
        if (Double.compare(that.prixAchat, prixAchat) != 0) return false;
        if (nomArticle != null ? !nomArticle.equals(that.nomArticle) : that.nomArticle != null) return false;
        if (volume != null ? !volume.equals(that.volume) : that.volume != null) return false;
        if (titrage != null ? !titrage.equals(that.titrage) : that.titrage != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = idArticle;
        result = 31 * result + (nomArticle != null ? nomArticle.hashCode() : 0);
        temp = Double.doubleToLongBits(prixAchat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (titrage != null ? titrage.hashCode() : 0);
        return result;
    }
}

package job;

import javax.persistence.*;

@Entity
@Table(name = "VENDRE", schema = "dbo", catalog = "SDBM")
@IdClass(VendreEntityPK.class)
public class VendreEntity {
    private int annee;
    private int numeroTicket;
    private int idArticle;
    private Integer quantite;

    @Id
    @Column(name = "ANNEE")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Id
    @Column(name = "NUMERO_TICKET")
    public int getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(int numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    @Id
    @Column(name = "ID_ARTICLE")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Basic
    @Column(name = "QUANTITE")
    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendreEntity that = (VendreEntity) o;

        if (annee != that.annee) return false;
        if (numeroTicket != that.numeroTicket) return false;
        if (idArticle != that.idArticle) return false;
        if (quantite != null ? !quantite.equals(that.quantite) : that.quantite != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = annee;
        result = 31 * result + numeroTicket;
        result = 31 * result + idArticle;
        result = 31 * result + (quantite != null ? quantite.hashCode() : 0);
        return result;
    }
}

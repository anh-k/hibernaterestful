package job;

import javax.persistence.*;

@Entity
@Table(name = "FABRICANT", schema = "dbo", catalog = "SDBM")
public class FabricantEntity {
    private int idFabricant;
    private String nomFabricant;

    @Id
    @Column(name = "ID_FABRICANT")
    public int getIdFabricant() {
        return idFabricant;
    }

    public void setIdFabricant(int idFabricant) {
        this.idFabricant = idFabricant;
    }

    @Basic
    @Column(name = "NOM_FABRICANT")
    public String getNomFabricant() {
        return nomFabricant;
    }

    public void setNomFabricant(String nomFabricant) {
        this.nomFabricant = nomFabricant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FabricantEntity that = (FabricantEntity) o;

        if (idFabricant != that.idFabricant) return false;
        if (nomFabricant != null ? !nomFabricant.equals(that.nomFabricant) : that.nomFabricant != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idFabricant;
        result = 31 * result + (nomFabricant != null ? nomFabricant.hashCode() : 0);
        return result;
    }
}

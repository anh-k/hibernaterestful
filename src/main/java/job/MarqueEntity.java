package job;

import javax.persistence.*;

@Entity
@Table(name = "MARQUE", schema = "dbo", catalog = "SDBM")
public class MarqueEntity {
    private int idMarque;
    private String nomMarque;

    @Id
    @Column(name = "ID_MARQUE")
    public int getIdMarque() {
        return idMarque;
    }

    public void setIdMarque(int idMarque) {
        this.idMarque = idMarque;
    }

    @Basic
    @Column(name = "NOM_MARQUE")
    public String getNomMarque() {
        return nomMarque;
    }

    public void setNomMarque(String nomMarque) {
        this.nomMarque = nomMarque;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarqueEntity that = (MarqueEntity) o;

        if (idMarque != that.idMarque) return false;
        if (nomMarque != null ? !nomMarque.equals(that.nomMarque) : that.nomMarque != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idMarque;
        result = 31 * result + (nomMarque != null ? nomMarque.hashCode() : 0);
        return result;
    }
}

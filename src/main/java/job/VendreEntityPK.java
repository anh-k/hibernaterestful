package job;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class VendreEntityPK implements Serializable {
    private int annee;
    private int numeroTicket;
    private int idArticle;

    @Column(name = "ANNEE")
    @Id
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Column(name = "NUMERO_TICKET")
    @Id
    public int getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(int numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    @Column(name = "ID_ARTICLE")
    @Id
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendreEntityPK that = (VendreEntityPK) o;

        if (annee != that.annee) return false;
        if (numeroTicket != that.numeroTicket) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = annee;
        result = 31 * result + numeroTicket;
        result = 31 * result + idArticle;
        return result;
    }
}

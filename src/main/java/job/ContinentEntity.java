package job;

import javax.persistence.*;

@Entity
@Table(name = "CONTINENT", schema = "dbo", catalog = "SDBM")
public class ContinentEntity {
    private int idContinent;
    private String nomContinent;

    @Id
    @Column(name = "ID_CONTINENT")
    public int getIdContinent() {
        return idContinent;
    }

    public void setIdContinent(int idContinent) {
        this.idContinent = idContinent;
    }

    @Basic
    @Column(name = "NOM_CONTINENT")
    public String getNomContinent() {
        return nomContinent;
    }

    public void setNomContinent(String nomContinent) {
        this.nomContinent = nomContinent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContinentEntity that = (ContinentEntity) o;

        if (idContinent != that.idContinent) return false;
        if (nomContinent != null ? !nomContinent.equals(that.nomContinent) : that.nomContinent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idContinent;
        result = 31 * result + (nomContinent != null ? nomContinent.hashCode() : 0);
        return result;
    }
}

package job;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "TICKET", schema = "dbo", catalog = "SDBM")
@IdClass(TicketEntityPK.class)
public class TicketEntity {
    private int annee;
    private int numeroTicket;
    private Timestamp dateVente;

    @Id
    @Column(name = "ANNEE")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Id
    @Column(name = "NUMERO_TICKET")
    public int getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(int numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    @Basic
    @Column(name = "DATE_VENTE")
    public Timestamp getDateVente() {
        return dateVente;
    }

    public void setDateVente(Timestamp dateVente) {
        this.dateVente = dateVente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketEntity that = (TicketEntity) o;

        if (annee != that.annee) return false;
        if (numeroTicket != that.numeroTicket) return false;
        if (dateVente != null ? !dateVente.equals(that.dateVente) : that.dateVente != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = annee;
        result = 31 * result + numeroTicket;
        result = 31 * result + (dateVente != null ? dateVente.hashCode() : 0);
        return result;
    }
}
